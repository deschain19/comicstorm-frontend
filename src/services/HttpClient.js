import axios from 'axios';

// Create a new instance.
const service = axios.create({
  baseURL: process.env.API_ENDPOINT,
  delayed: true,
  time:0  // use this custom option to allow overrides
});



service.interceptors.request.use((config) =>{
  if(config.delayed) {
    return new Promise(resolve => setTimeout(() => resolve(config), config.time));
  }
  return config;
});

export default service;