import { createRouter, createWebHistory } from "vue-router";
import Dashboard from '@/views/Dashboard.vue'
import Items from '@/views/item/Items.vue'

import Mangas from '@/views/manga/Mangas.vue'
import Kindles from '@/views/kindle/Kindles.vue'
import AudioBooks from '@/views/audiobook/AudioBooks.vue'

/**Comics */
import Comics from '@/views/comic/Comics.vue'
/*Books */
import Books from '@/views/book/Books.vue'
/**Authentication */
import Login from '@/authentication/Login.vue'
/***************************Cart*********************************************/
import Cart from '@/views/cart/Cart.vue'
/***************************Items********************************************/
import ItemsAdd from '@/views/item/ItemsAdd.vue'
import ItemsShow from '@/views/item/ItemsShow.vue'
import HomeView from '@/views/HomeView.vue'
const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: HomeView,
  },
  {
    
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    children:[
      {
        path: '/articulos',
        component: Items,
        name:'items'       
      },
      {
        path: '/libros',
        component: Books,
        name:'books'       
      },
      {
        path: '/kindles',
        component: Kindles,
        name:'kindles'       
      },
      {
        path: '/mangas',
        component: Mangas,
        name:'mangas'       
      },
      {
        path: '/audio-libros',
        component: AudioBooks,
        name:'audiobooks'       
      },
      {
        path: '/comics',
        component: Comics,
        name:'comics'       
      },
      {
        path: '/login',
        component: Login,
        name:'login'       
      },
      
      {
        path: '/carrito',
        component: Cart,
        name:'cart'       
      },
   
      {
        path: '/agregar-articulos',
        component: ItemsAdd,
        name:'items-add'       
      },
      {
        path: '/ver-articulo/:id',
        component: ItemsShow,
        name:'items-show'       
      },
    ]
  },
];







const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
