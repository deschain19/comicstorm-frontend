import { createStore } from "vuex";
import axios from 'axios'
export default createStore({
  state: {
   
    cart:[],
    item:null,
    items:[],
    id:null
  },
  getters: {  
    getItem(state){
      return state.item
    }
  },
  mutations: {
    addAmigo( state ){
      state.items = [state.id,...state.items]
    }

  },
  actions: {
    addAmigoAction(context){
      context.commit('addAmigo')
    }
  },
  modules: {},
});
